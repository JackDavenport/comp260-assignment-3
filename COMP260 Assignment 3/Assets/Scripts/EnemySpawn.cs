﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {
	public EnemyMove EnemyPrefab;
	public float startX;
	public float endx;
	private int counter = 0;

	// Use this for initialization
	void Start () {
	}
	void  SpawnEnemy(){
		for (int i = 0; i < 2	; i++) {
			EnemyMove enemy = Instantiate (EnemyPrefab);
			enemy.transform.parent = transform;
			enemy.gameObject.name = "enemy " + i;
			float randomEnd = Random.Range (0.1f, 10.00f);
			enemy.EndY = randomEnd;
			enemy.endX = endx;
		}
	}
	// Update is called once per frame
	void Update () {
		counter = counter + 1;
		if (counter % 30 == 0 ) {
			counter = counter + 1;
			SpawnEnemy();
		}
	}
}
