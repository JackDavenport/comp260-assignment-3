﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {
	private AudioSource _audio;
	public LayerMask playerLayer;
	public LayerMask endLayer;
	public Transform player;
	public float speed =6.0f;
	public float EndY;//= 0.42f;
	public float endX;
	private bool isGone = false;
	public Vector2 startPos;
	// Use this for initialization
	void Start () {
		float newStart = Random.Range (0.1f, 10.00f);
		startPos.Set(0, newStart);
	
		transform.position = startPos;
		_audio = gameObject.GetComponent<AudioSource>();
	}

//	// Update is called once per frame
	void Update () {
		Vector3 newPos = new Vector2(endX, EndY);
		Vector2 direction = newPos - transform.position;
		direction = direction.normalized;
		Vector2 velocity = direction * speed;
		transform.Translate (velocity * Time.deltaTime);
	}
	public void OnTriggerEnter2D(Collider2D collider) {		
		if (isGone) return;

		// check if the colliding object is in the playerLayer
		if (((1 << collider.gameObject.layer) & playerLayer.value) != 0) {
			// play a sound if there is one
			if (_audio != null) {
				_audio.Play();
			}

			// hide the sprite and disable this script
			SpriteRenderer _renderer = gameObject.GetComponent<SpriteRenderer>();
			_renderer.enabled = false;
			isGone = true;

			// somehow it needs to call in the health script and then remove a health point. 
			//the health script then prints to the canvas, and also allows for the player health pick up to work
		}

		if (((1 << collider.gameObject.layer) & endLayer.value) != 0) {
			Destroy (gameObject);
		}
	}
}
