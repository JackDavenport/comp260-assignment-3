﻿using UnityEngine;
using System.Collections;

public class BoxMove : MonoBehaviour {
	public LayerMask endLayer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void OnTriggerEnter2D(Collider2D collider) {	
		if (((1 << collider.gameObject.layer) & endLayer.value) != 0) {
			Destroy (gameObject);
		}
	}
}
