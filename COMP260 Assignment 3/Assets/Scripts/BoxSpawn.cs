﻿using UnityEngine;
using System.Collections;

public class BoxSpawn : MonoBehaviour {
	public GameObject boxPre;
	public GameObject health;
	private int counter = 0;
	// Use this for initialization
	void Start () {
	}


	public void SpawnBox(){
		for (int i = 0; i < 2; i++) {
			GameObject box = Instantiate (boxPre);
			box.transform.parent = transform;
			box.gameObject.name = "box " + i;
			float StartX = Random.Range (0.48f, 17.29f);
			box.transform.position = new Vector2 (StartX, 10.55f);
		}
	}

	public void SpawnHealth(){
		GameObject healthPickup = Instantiate (health);
		healthPickup.transform.parent = transform;
		healthPickup.gameObject.name = "health";
		float StartX = Random.Range (0.48f, 17.29f);
		healthPickup.transform.position = new Vector2 (StartX, 10.55f);
	}
	// Update is called once per frame
	void Update () {
		counter = counter + 1;
		if (counter % 30 == 0 ) {
			counter = counter + 1;
			SpawnBox();
		}
	}
}
