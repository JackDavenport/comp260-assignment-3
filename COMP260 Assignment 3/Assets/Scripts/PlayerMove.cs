﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	
	public int playerHealth = 4;
	public LayerMask enemyLayer;
	public LayerMask healthLayer;
	public Text scoreHealth;
	public float DimXmin;
	public float DimXmax;
	public float DimYmin;
	public float DimYmax;
	public Vector2 StartPos;
	public Rigidbody2D rb;
	// Use this for initialization
	void Start () {
		transform.position = StartPos;
		scoreHealth.text = "Health : "+ playerHealth.ToString();
	}
	
	float maxSpeed = 5.0f;

	void Update() {
		if (playerHealth == 0) { //with no health the player is not able to move
			maxSpeed = 0.0f;
			scoreHealth.text = "GAME OVER";
		}
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");
			Vector2 velocity = direction * maxSpeed;
			transform.Translate (velocity * Time.deltaTime);
	}
	public void OnTriggerEnter2D(Collider2D collider) {	
		if (((1 << collider.gameObject.layer) & healthLayer.value) != 0) { //if it collides with a object in the health layer it adds a health point
			if (playerHealth > 0) {
				playerHealth = playerHealth + 1;
				scoreHealth.text = "Health : " + playerHealth.ToString ();
			} else {
				scoreHealth.text = "GAME OVER";
			}
		}	
		if (((1 << collider.gameObject.layer) & enemyLayer.value) != 0) { // if player collides with object on enemy layer it looses a health point
			if (playerHealth > 0) {
				playerHealth = playerHealth - 1;
				scoreHealth.text = "Health : " + playerHealth.ToString ();
			} else {
				scoreHealth.text = "GAME OVER";
			}
		}
	}
}
