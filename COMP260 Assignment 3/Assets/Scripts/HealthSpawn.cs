﻿using UnityEngine;
using System.Collections;

public class HealthSpawn : MonoBehaviour {

	public GameObject healthPre;
	public int counter = 0;
	// Use this for initialization
	void Start () {
	
	}
	public void SpawnBox(){
		GameObject box = Instantiate (healthPre);
			box.transform.parent = transform;
			box.gameObject.name = "box ";
			float StartX = Random.Range (0.48f, 17.29f);
			box.transform.position = new Vector2 (StartX, 10.55f);
	}
	// Update is called once per frame
	void Update () {
		counter = counter + 1;
		if (counter % 150 == 0 ) {
			counter = counter + 1;
			SpawnBox();
		}
	}
}
