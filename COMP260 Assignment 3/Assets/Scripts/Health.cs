﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	public LayerMask endLayer;
	public LayerMask playerLayer;
	//private AudioSource healthUp; - Doesnt work
	private bool isGone = false;
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	void Update () {
	}

	public void OnTriggerEnter2D(Collider2D collider) {	
		if (isGone) return;
		if (((1 << collider.gameObject.layer) & endLayer.value) != 0) {
			Destroy (gameObject);
		}
		// check if the colliding object is in the playerLayer
		if (((1 << collider.gameObject.layer) & playerLayer.value) != 0) {
			// hide the sprite and disable this script
			SpriteRenderer _renderer = gameObject.GetComponent<SpriteRenderer>();
			_renderer.enabled = false;
			isGone = true;
		}
	}

}
